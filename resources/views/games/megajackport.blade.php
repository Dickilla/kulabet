@extends('member.layout.auth')

@section('content')

<section >
     <div class="container topspacing">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="text-center"><u><b>Mega Jackport</b></u></h2>
            </div>
            <div class="col-md-12 text-center">            
              <div class="col-md-2"></div>              
              <div class="col-md-8">
                <div class="alert alert-info" role="alert">All mega jackport matches are posted here  every saturday morning</div>
              </div>         
              <div class="col-md-2"></div>  
            </div>
          </div>
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($contents) >= 1)
                                      @foreach($contents as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('details', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }} </a></td>
                                          <td> <a href="{{ route('details', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;VIEW</a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No megajackport Matches today</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   


@endsection