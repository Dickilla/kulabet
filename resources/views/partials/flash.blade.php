@if(session('status'))
	 <div class="alert alert-success alert-dismissible fade-in ${dismissed ? 'hidden': ''}" role="alert">
		<button type="button" class="close" click.delegate="dismiss" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong>&nbsp;&nbsp;&nbsp;{{ session('status') }}
	</div>
@elseif(session('statuswrong'))
	 <div class="alert alert-danger alert-dismissible fade-in ${dismissed ? 'hidden': ''}" role="alert">
	    <button type="button" class="close" click.delegate="dismiss" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	    </button>
	    <strong>Failed!</strong>&nbsp;&nbsp;&nbsp;{{ session('statuswrong') }}
	</div>
@endif
