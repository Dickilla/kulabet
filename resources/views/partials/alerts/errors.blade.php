@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <ul>
              <strong><li class="text-center">{{ $error }}</li></strong>
            </ul>
        @endforeach
    </div>
@endif
