@extends('member.layout.auth')

@section('content')
    <!--  section 1  -->
    <section>
        <div class="container">
            <div class="row topspacing">
                <div class="col-md-7">
                    <h3 style="color:#FF0000;">Looking for Tips? </h3>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;Discover the world of sports investment and see how to increase your profits</h4>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;We deliver the best football predictions available</h4>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;Without data, your are just but another gambler</h4>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;Today's popular matches predictions</h4>

                    <img src="img/messi.jpg" alt="Generic placeholder image" style="margin-right:15px;" height="300" width="650" class="img-responsive">
                </div>
                <div id="sidebar" class="col-md-5">
                    <div class="panel panel-success">
                        <div class="regheader">
                            <h3 class="panel-title" align="center"><b>REGISTER HEREe</b></h3>
                        </div>
                        <div id="sidenav" class="panel-body regbody">
                            <form role="form" method="post" name="add_terminal" id="add_terminal" class="form-horizontal">
                              <div class="form-group">
                                <label  class="col-md-4 control-label">First Namee</label>
                                <div class="col-md-8">
                                  <input name="tname" id="tname" type="text" class="form-control" placeholder="First Name...">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Last Namee</label>
                                <div class="col-md-8">
                                  <input name="tid" id="tid" type="text" class="form-control" placeholder="Last Name...">
                                </div>
                              </div> 
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Phone Number</label>
                                <div class="col-md-8">
                                  <input name="contacts" id="contacts" type="text" class="form-control" placeholder="Phone Number">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Email Address</label>
                                <div class="col-md-8">
                                  <input name="contacts" id="contacts" type="text" class="form-control" placeholder="Email Address">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Password</label>
                                <div class="col-md-8">
                                  <input name="contacts" id="contacts" type="text" class="form-control" placeholder="Password">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-8">
                                  <input name="contacts" id="contacts" type="text" class="form-control" placeholder="Confirm Password">
                                </div>
                              </div>
                              
                              <div class="col-md-12 col-md-offset-0" align="right">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save "></span><b>SUBMIT</b></button>
                                <a class="btn btn-link" href="#">
                                    <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-save "></span><b>Login</b></button>
                                </a>
                              </div>
                          </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--  end section 1  -->
    <!--  Section 2 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Today's Free Tips</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($free) >= 1)
                                      @foreach($free as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('details', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }} </a></td>
                                          <td> <a href="{{ route('details', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;VIEW</a>
                                          </td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Free Tips Today</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 2  -->
    <!--  Section 3 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Popular Matches</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($popular) >= 1)
                                      @foreach($popular as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('details', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }} </a></td>
                                          <td> <a href="{{ route('details', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;VIEW</a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Popular Matches</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 3  -->
    <!--  Section 4 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Latest Won Results</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($won) >= 1)
                                      @foreach($won as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td><a href="{{ route('details', $content->gameID) }}"> {{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td> <a href="{{ route('details', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-edit">&nbsp;VIEW</a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Latest Won Results</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 4  -->
@endsection