@extends('admin.layout.auth')

@section('content')

	<section>
    <div class="container colorcontainer topspacing">   
      <div class="row  pushdown">
       <div id="sidebar" class="col-md-3">

       </div>
        
	      <div class="col-md-6">
	        
	           <div id="welcome" class="panel panel-success contactback">
			         <div class="panel-heading panelcala">
			          	<h3 class="panel-title text-center"><b>Add Game</b></h3>
			         </div>
			         <div class="panel-body">

			          	        <form role="form" method="post" action="cont/{{ $content->gameID }}/update" class="form-horizontal">
			          	 	          {{ csrf_field() }}
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Country</label>
                                <div class="col-md-8">
                                  <input name="country" id="country" value="{{ $content->country }}" type="text" class="form-control" >
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">League</label>
                                <div class="col-md-8">
                                  <input name="league" id="league" value="{{ $content->league }}" type="text" class="form-control" >
                                </div>
                              </div> 
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Team A</label>
                                <div class="col-md-8">
                                  <input name="teamone" id="teamone" value="{{ $content->teamone }}" type="text" class="form-control">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Team B</label>
                                <div class="col-md-8">
                                  <input name="teamtwo" id="teamtwo" value="{{ $content->teamtwo }}" type="text" class="form-control">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Tip</label>
                                <div class="col-md-8">                                 
                                 <select name="category" id="category" type="text" class="form-control" >
                                          <option value="{{ $content->category }}">{{ $content->category }}</option>
                                        @foreach($category as $cat)                                          
                                          <option value="{{ $cat->cat_alias }}">{{ $cat->cat_alias }}...{{ $cat->cat_name }}</option>
                                        @endforeach
                                 </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Results</label>
                                <div class="col-md-8">
                                  <input name="outcome" id="outcome" value="{{ $content->outcome }}" type="text" class="form-control">
                                </div>
                              </div>
                              
                              <div class="col-md-12 col-md-offset-0" align="right">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save "></span><b>UPDATE</b></button>
                              </div>
                          </form>

			         </div>
	         	</div>
	        </div>
	        <div id="sidebar" class="col-md-3">

	        </div>
        
      </div>
    </div>
</section>

@endsection

