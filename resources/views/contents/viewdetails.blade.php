@extends('member.layout.auth')

@section('content')

<section >
    <div class="container topspacing">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><b>Match Analysis</b></h1>
			</div>
			<div class="col-md-3">
				
			</div>				   	
            <div class="col-md-6">
                <div class="thumbnail">
                    
                    <div class="caption">
                    	<div class="table-responsive-force">
                 				<table class="table table-condensed ">
                                  <tbody>
                                  	<tr>
                                  		<th>TEAMS:</th>
                                  		<td class="text-center">{{ $content->teamone }}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; VS<br> {{ $content->teamtwo }}</td>	
                                  	</tr> 
                                  	<tr>
                                  	    <th>LEAGUE CODE:</th>
                                  		<td>{{ $content->league }}</td>	
                                  	</tr>
                                  	<tr>
                                  	    <th>LEAGUE NAME:</th>
                                  		<td>{{ $content->league }}</td>	
                                  	</tr>
                                  	<tr>
                                  		<th>COUNTRY:</th>	
                                  		<td>{{ $content->country }}</td>
                                  	</tr>
                                  	<tr>
                                  	    <th>PREDICTION:</th>	
                                  		<td>{{ $content->category }}</td>
                                  	</tr>
                                  	<tr>
                                  	    <th>MEANING:</th>	
                                  		<td>{{ $cat->cat_name }}</td>
                                  	</tr>
                                  </tbody>
                                </table>
                 		</div>
                    </div>
                </div>
            </div>	
			<div class="col-md-3">

			</div>			  
		</div>
	</div>
</section>   

@endsection