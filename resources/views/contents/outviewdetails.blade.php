@extends('member.layout.auth')

@section('content')

<section >
    <div class="container topspacing">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><b>Match Analysis</b></h1>
			</div>
			<div class="col-md-3">
				
			</div>				   	
            <div class="col-md-6">
                <div class="thumbnail">
                    
                    <div class="caption">
                    	<div class="table-responsive-force">
                 				<table class="table">
                                  <tbody>
                                  	<tr>
                                  		<th>TEAMS:</th>
                                  		<th >{{ $content->teamone }}<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VS<br> {{ $content->teamtwo }}</th>	
                                  	</tr> 
                                  	<tr>
                                  	    <th>LEAGUE CODE:</th>
                                  		<th>{{ $content->league }}</th>	
                                  	</tr>
                                  	<tr>
                                  	    <th>LEAGUE NAME:</th>
                                  		<th>{{ $content->league }}</th>	
                                  	</tr>
                                  	<tr>
                                  		<th>COUNTRY:</th>	
                                  		<th>{{ $content->country }}</th>
                                  	</tr>
                                  	<tr>
                                  	    <th>PREDICTION:</th>	
                                  		<th>{{ $content->category }}</th>
                                  	</tr>
                                  	<tr>
                                  	    <th>MEANING:</th>	
                                  		<th>{{ $cat->cat_name }}</th>
                                  	</tr>
                                  </tbody>
                                </table>
                 		</div>
                    </div>
                </div>
            </div>	
			<div class="col-md-3">

			</div>			  
		</div>
	</div>
</section>   

@endsection