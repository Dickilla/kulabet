@extends('admin.layout.auth')

@section('content')

	<section>
    <div class="container colorcontainer topspacing">   
      <div class="row  pushdown">
       <div id="sidebar" class="col-md-3">

       </div>
        
	      <div class="col-md-6">
	        
	           <div id="welcome" class="panel panel-success contactback">
			         <div class="panel-heading panelcala">
			          	<h3 class="panel-title text-center"><b>Add Tip</b></h3>
			         </div>
			         <div class="panel-body">
			          	 <form role="form" method="post" action="createtip" class="form-horizontal" id="addtip">
			          	 	{{ csrf_field() }}
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Tip Name</label>
                                <div class="col-md-8">
                                  <input name="cat_name" id="cat_name" type="text" class="form-control" placeholder="Tip Name">
                                  @if ($errors->has('cat_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cat_name') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Tip</label>
                                <div class="col-md-8">
                                  <input name="cat_alias" id="cat_alias" type="text" class="form-control" placeholder="Tip">
                                  @if ($errors->has('cat_alias'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cat_alias') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              
                              <div class="col-md-12 col-md-offset-0" align="right">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save "></span><b>ADD</b></button>
                              </div>
                          </form>

			         </div>
	         	</div>
	        </div>        
      </div>
    </div>
</section>

@endsection

