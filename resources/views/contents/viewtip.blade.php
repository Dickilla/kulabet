@extends('admin.layout.auth')

@section('content')

<section >
     <div class="container topspacing">
	      @if(Session::has('flash_message'))
	        <div class="alert alert-success">
	            {{ Session::get('flash_message') }}
	        </div>
	      @endif
		  <div class="row">
			 <div id="sidebar" class="col-md-1">
				
			</div>
				
			<div class="col-md-10">
			  
	         <div id="welcome" class="panel panel-success">
			   <div class="panel-heading">
				  <h3 class="panel-title text-center"><b>View Tips</b></h3>
			   </div>
			   <div class="panel-body">
			     <div class="table-responsive-force">
			     <table class="table table-bordered table-hover table-striped tablesorter viewtip">
								  <thead>
									<tr>
									  <th>Tip Name</th>
									  <th>Tip</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	
									@foreach($tip as $tip)
									<tr>
										<td> {{ $tip->cat_name }}  </td>
										<td> {{ $tip->cat_alias }}  </td>
										<td>
											<form role="form" method="post" action="deletetip/{{ $tip->id }}" class="form-horizontal">
												{{ csrf_field() }}
												<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash "></span><b>Delete</b></button>
											</form>
										</td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>
				 </div>						
			   </div>
			   </div>
			
			   
			  </div>
			  <div id="sidebar" class="col-md-1">
				
			</div>
			  
			 </div>
		</div>
	</section>   


@endsection