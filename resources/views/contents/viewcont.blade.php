@extends('admin.layout.auth')

@section('content')

<section >
     <div class="container topspacing">
	      @if(Session::has('flash_message'))
	        <div class="alert alert-success">
	            {{ Session::get('flash_message') }}
	        </div>
	      @endif
		  <div class="row">
			 <div id="sidebar" class="col-md-1">
				
			</div>
				
			<div class="col-md-10">
			  
	         <div id="welcome" class="panel panel-success">
			   <div class="panel-heading">
				  <h3 class="panel-title text-center"><b>Games</b></h3>
			   </div>
			   <div class="panel-body">
			     <div class="table-responsive-force">
			     <table class="table table-bordered table-hover table-striped tablesorter viewcont">
								  <thead>
									<tr>
									  <th>Country</th>
									  <th>League</th>
									  <th>Teams</th>
									  <th>Tip</th>
									  <th>Results</th>
									  <th>Action</th>
									  <th>Action</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	
									@foreach($contents as $content)
									<tr>
										<td> {{ $content->country }}  </td>
										<td> {{ $content->league }}  </td>
										<td> {{ $content->teamone }} vs {{ $content->teamtwo }} </td>
										<td> {{ $content->category }}  </td>
										<td> {{ $content->outcome }}  </td>
										<td> <a href="{{ route('edit_cont', $content->gameID) }}" class="btn btn-primary"><span class="glyphicon glyphicon-edit">&nbsp;Edit</a></td>
										<td>
				                            @if($content->hide == 0)
				                            	<a href="{{ route('unhide', $content->gameID) }}" 
				                             class="btn btn-success" ><span class="glyphicon glyphicon-ok-circle"></span> Hide </a>
				                            				                           
				                            @else	 
				                            	<a href="{{ route('hide', $content->gameID) }}" 
				                             class="btn btn-primary" ><span class="glyphicon glyphicon-remove-circle"></span> Show </a>
											@endif
				                          
				                        </td>
										<td>
											<form role="form" method="post" action="delete/{{ $content->gameID }}" class="form-horizontal">
												{{ csrf_field() }}
												<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash "></span><b>Delete</b></button>
											</form>
										</td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>
				 </div>						
			   </div>
			   </div>
			
			   
			  </div>
			  <div id="sidebar" class="col-md-1">
				
			</div>
			  
			 </div>
		</div>
	</section>


@endsection