@extends('admin.layout.auth')

@section('content')

<section>
  <div class="container colorcontainer topspacing">   
  <div class="row  pushdown">
  <div id="sidebar" class="col-md-3">

</div>
        
	      <div class="col-md-6">
	        
	           <div id="welcome" class="panel panel-success contactback">
			         <div class="panel-heading panelcala">
			          	<h3 class="panel-title text-center"><b>Add Game</b></h3>
			         </div>
			         <div class="panel-body">
			          	 <form role="form" method="post" action="createcont" class="form-horizontal" id="addcont">
			          	 	{{ csrf_field() }}
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Country</label>
                                <div class="col-md-8">
                                  <input name="country" id="country"  value="{{ old('country') }}" type="text" class="form-control" placeholder="Country">
                                  @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">League</label>
                                <div class="col-md-8">
                                  <input name="league" id="league" type="text" value="{{ old('league') }}" class="form-control" placeholder="League">
                                  @if ($errors->has('league'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('league') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div> 
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Team A</label>
                                <div class="col-md-8">
                                  <input name="teamone" id="teamone" type="text" value="{{ old('teamone') }}" class="form-control" placeholder="Team">
                                  @if ($errors->has('teamone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('teamone') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Team B</label>
                                <div class="col-md-8">
                                  <input name="teamtwo" id="teamtwo" type="text" value="{{ old('teamtwo') }}" class="form-control" placeholder="Team">
                                  @if ($errors->has('teamtwo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('teamtwo') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Tip</label>
                                <div class="col-md-8">
                                 <select name="category" id="category" type="text" value="{{ old('category') }}" class="form-control" >
                                        @foreach($category as $cat)                                          
                                          <option value="{{ $cat->cat_alias }}">{{ $cat->cat_alias }}...{{ $cat->cat_name }}</option>
                                        @endforeach
</select>
</div>
  </div>
                              
<div class="col-md-12 col-md-offset-0" align="right">
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save "></span><b>ADD</b></button>
</div>
</form>

	</div>
	</div>
	</div>
 <div id="sidebar" class="col-md-3">
            
  </div>
        
 </div>
 </div>
</section>

@endsection

