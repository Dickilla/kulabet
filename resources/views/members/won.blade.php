@extends('member.layout.auth')

@section('content')

<section >
     <div class="container topspacing">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="text-center"><u><b>Won Matches</b></u></h2>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">
            </div>
            <div class="col-md-1"></div>
          </div>
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Tip</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($contents) >= 1)
                                      @foreach($contents as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('details', $content->gameID) }}"> {{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td><a href="{{ route('details', $content->gameID) }}"> {{ $content->category }} </a> </td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Latest Won Results</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   


@endsection