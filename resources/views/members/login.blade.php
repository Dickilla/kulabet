@extends('layouts.masterlogreg')

@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

@section('content')

	<section>
    <div class="container colorcontainer topspacing">   
      <div class="row  pushdown">
       <div id="sidebar" class="col-md-3">

       </div>
        
	      <div class="col-md-6">
	        
	           <div id="welcome" class="panel panel-success contactback">
			         <div class="panel-heading panelcala">
			          	<h3 class="panel-title text-center"><b>Member Login</b></h3>
			         </div>
			         <div class="panel-body">
			          	 <form role="form" method="post" action="membercheck" class="form-horizontal">
			          	 	{{ csrf_field() }}
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                  <input name="email" id="email" type="text" class="form-control" placeholder="Email">
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Password</label>
                                <div class="col-md-8">
                                  <input name="password" id="password" type="password" class="form-control" placeholder="password">
                                </div>
                              </div>
                              
                              <div class="col-md-12 col-md-offset-0" align="right">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in fa-1x"></i>&nbsp;<b>Login</b></button>
                              </div>
                          </form>

			         </div>
	         	</div>
	        </div>
	        <div id="sidebar" class="col-md-3">

	        </div>
        
      </div>
    </div>
</section>

@endsection

