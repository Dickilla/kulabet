@extends('member.layout.auth')

@section('content')
    <!--  section 1  -->
    <section>
        <div class="container">
            <div class="row topspacing">
                <div class="col-md-7 text-center" >
                  <div class="thumbnail">
                    <h3 style="color:blue;">Looking for Tips? </h3>
                    <h4>Please Make a  <span style="color:#FF0000;font-size:20px"><b>MONTHLY</b></span>subscription of <span style="color:#FF0000;font-size:20px"><b>ksh 550</b></span> only to <b><span style="color:#FF0000;font-size:20px">MPESA</span></b> till no.<span style="color:#FF0000;font-size:20px"><b>797925</b></span> To Be  Able to Access daily matches.
                    Many people around the WORLD win good MONEY through our predictions, You can be one of them.To all foreign buyers contact through email.Pay by western union.</h4>
                    <p class="text-center"><img src="img/image1.jpg" alt="Generic placeholder image" /></p>
                    <p>After paying through our paybill,send the mpesa transaction number to <span style="color:#FF0000;font-size:20px">0713609450</span> to be activated </p>
                  </div>
                </div>
                <div id="sidebar" class="col-md-5 text-center">                   
                  <div class="thumbnail">
                      <img class="img-responsive" src="img/mpesa.png" alt="">
                      <div >
                          <h3>How to pay</h3>
                              <p>From your Mobile Phone.<br>
                                Go to 'Safaricom' Menu<br>
                                Select 'buy goods $ services', click Ok<br>
                                Enter 'Out till no 797925', click OK,<br>
                                Input Amount. ksh 550 click 'OK'<br>
                                Enter your PIN Number. click OK<br>
                                Confirm by clicking OK<br>
                                You will receive a confirmation code from safaricom<br>
                                Sent that code to <span style="color:#FF0000;font-size:20px">0713609450</span><br>
                                to be activated.
                              </p>
                      </div>
                  </div>                    
                </div>
            </div>
        </div>
    </section>
    <!--  end section 1  -->
    <!--  Section 2 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Today's Free Tips</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($free) >= 1)
                                      @foreach($free as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-eye-open">&nbsp;VIEW</a>
                                          </td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Free Tips Today</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 2  -->
    <!--  Section 3 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Popular Matches</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($popular) >= 1)
                                      @foreach($popular as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('memreg') }}">{{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td> <a href="{{ route('memreg') }}" class="btn btn-success"><span class="glyphicon glyphicon-eye-open">&nbsp;VIEW</a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Popular Matches</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 3  -->
    <!--  Section 4 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Latest Won Results</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Tip</th>
                                      <th>Results</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($won) >= 1)
                                      @foreach($won as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td> {{ $content->category }}  </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Latest Won Results</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   
    <!-- End section 4  -->
@endsection