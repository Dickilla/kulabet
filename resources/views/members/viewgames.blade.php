@extends('member.layout.auth')

@section('content')

<section class="admin" id="admin">
     <div class="container topspacing">
		  <div class="row">
			 <div id="sidebar" class="col-md-1">
				
			</div>
				
			<div class="col-md-10">
			  
	         <div id="welcome" class="panel panel-success">
			   <div class="panel-heading">
				  <h3 class="panel-title"><b>Games</b></h3>
			   </div>
			   <div class="panel-body">
			     <div class="table-responsive-force">
			     <table class="table table-bordered table-hover table-striped tablesorter">
								  <thead>
									<tr>
									  <th>Country</th>
									  <th>League</th>
									  <th>Teams</th>
									  <th>Tip</th>
									</tr>
								  </thead>
								  <tbody>
								  	
									@foreach($contents as $content)
									<tr>
										<td> {{ $content->country }}  </td>
										<td> {{ $content->league }}  </td>
										<td> {{ $content->teamone }} vs {{ $content->teamtwo }} </td>
										<td> {{ $content->category }}  </td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>
				 </div>						
			   </div>
			   </div>
			
			   
			  </div>
			  <div id="sidebar" class="col-md-1">
				
			</div>
			  
			 </div>
		</div>
	</section>   


@endsection