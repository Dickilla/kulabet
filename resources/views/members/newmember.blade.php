@extends('member.layout.auth')

@section('content')

	<section>
    <div class="container colorcontainer topspacing">   
      <div class="row  pushdown">
       <div id="sidebar" class="col-md-3">

       </div>
        
	      <div class="col-md-6">
	        
	           <div id="welcome" class="panel panel-success contactback">
			         <div class="panel-heading panelcala">
			          	<h3 class="panel-title text-center"><b>REGISTER HERE</b></h3>
			         </div>
			         <div class="panel-body">
			          	 <form role="form" method="post" action="createmember" class="form-horizontal">
			          	 	{{ csrf_field() }}
                              <div class="form-group">
                                <label  class="col-md-4 control-label">First Name</label>
                                <div class="col-md-8">
                                  <input name="firstname" id="firstname" value="{{ old('firstname') }}" type="text" class="form-control" placeholder="firstname">
                                  @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                  @endif

                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Last Name</label>
                                <div class="col-md-8">
                                  <input name="lastname" id="lastname" value="{{ old('lastname') }}" type="text" class="form-control" placeholder="lastname">
                                  @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div> 
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Phone Number</label>
                                <div class="col-md-8">
                                  <input name="tel" id="tel" type="text" value="{{ old('tel') }}" class="form-control" placeholder="Phone number">
                                  @if ($errors->has('tel'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div><div class="form-group">
                                <label  class="col-md-4 control-label">Email</label>
                                <div class="col-md-8">
                                  <input name="email" id="email" value="{{ old('email') }}" type="text" class="form-control" placeholder="Email">
                                  @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Password</label>
                                <div class="col-md-8">
                                  <input name="password" id="password" type="password" class="form-control" placeholder="password">
                                  @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-8">
                                  <input name="password-confirm" id="password-confirm" type="text" class="form-control" placeholder="Confirm Password">
                                  @if ($errors->has('password-confirm'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                                  @endif
                                </div>
                              </div>
                              
                              <div class="col-md-12 col-md-offset-0" align="right">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save "></span><b>Register</b></button>
                              </div>
                          </form>

			         </div>
	         	</div>
	        </div>
	        <div id="sidebar" class="col-md-3">

	        </div>
        
      </div>
    </div>
</section>

@endsection

