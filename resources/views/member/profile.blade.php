@extends('member.layout.auth')

@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        @if(Session::has('flash_message'))
          <div class="alert alert-success">
              {{ Session::get('flash_message') }}
          </div>
        @endif
          <ul id="myTab" class="nav nav-tabs">
            <li class="active"><a href="#profile" data-toggle="tab" style="color:green"><i class="fa fa-book"></i>&nbsp; &nbsp;Profile</a>
            </li>
            <li class=""><a href="#editprofile" data-toggle="tab" style="color:green"><i class="fa fa-book"></i>&nbsp; &nbsp;Edit profile</a>
            </li>
          </ul>
          <div id="myTabContent" class="tab-content" ><!--Start tab content-->
        <div class="tab-pane fade active in" id="profile"><!--Start of inside tab content-->
          <div id="" class="panel panelcala"><!--Start of panel-->
           <div class="panel-heading regheader">
            <h3 class="panel-title text-center"><b>Profile</b></h3>
           </div>
           <div class="panel-body regbody"><!--Start of panel body-->
                <div class="col-md-12 text-center">
                  <h1><b>{{ $member->firstname }} &nbsp;{{ $member->lastname }}</b></h1>
                </div>           
                          <div class="thumbnail">
                              
                              <div class="caption">
                                <div class="table-responsive-force">
                                  <table class="table table-condensed ">
                                            <tbody> 
                                              <tr>
                                                  <th>Email:</th>
                                                <td>{{ $member->email }}</td>  
                                              </tr>
                                              <tr>
                                                  <th>Mobile Number:</th>
                                                <td>{{ $member->tel }}</td>  
                                              </tr>
                                              <tr>
                                                  <th>Subscription Ends:</th> 
                                                <td>
                                                  @if($current >=  $dx)
                                                    <p>Expired</p>                                 
                                                  @else
                                                    <p>{{ $dt }}</p>
                                                  @endif
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                      </div>
                                      </div>
                                  </div>   
                </div><!--End of panel body-->
            </div><!--End of panel-->                              
        </div>  <!--End of inside tab content--> 
        <div class="tab-pane" id="editprofile"><!--Start of inside tab content-->
          <div id="" class="panel panelcala "><!--Start of panel-->
           <div class="panel-heading regheader">
            <h3 class="panel-title text-center"><b>Edit Profile</b></h3>
           </div>
           <div class="panel-body regbody"><!--Start of panel body-->
               <!--<form role="form" method="post" action="memberprofile/{{ $member->id }}/update"}} class="form-horizontal">-->
                      {!! Form::model($member, ['method' => 'POST','route' => ['memberupdate', $member->id] ]) !!}
                              {{ csrf_field() }}
                              <div class="form-group">
                                <label  class="col-md-4 control-label" for="firstname">First Name</label>
                                <div class="col-md-8">
                                  <input name="firstname" id="firstname" type="text" value="{{ $member->firstname }}" class="form-control" placeholder="firstname">
                                  
                                </div>
                              </div>

                              <div class="form-group">
                                <label  class="col-md-4 control-label" for="lastname">Last Name</label>
                                <div class="col-md-8">
                                  <input name="lastname" id="lastname" type="text" value="{{ $member->lastname }}" class="form-control" placeholder="lastname">
                                  
                                </div>
                              </div> 
                              <div class="form-group">
                                <label  class="col-md-4 control-label for="tel"">Phone Number</label>
                                <div class="col-md-8">
                                  <input name="tel" id="tel" type="text" value="{{ $member->tel }}" class="form-control" placeholder="Phone number">
                                  
                                </div>
                              </div>
                              <div class="form-group">
                                <label  class="col-md-4 control-label" for="email">Email</label>
                                <div class="col-md-8">
                                  <input name="email" id="email" value="{{ $member->email }}" type="text" class="form-control" placeholder="Email">
                                  
                                </div>
                              </div>
                              
                              <div class="col-md-12 col-md-offset-0" align="right">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save "></span><b>UPDATE</b></button>
                              </div>
                          </form>
           </div><!--End of panel body-->
          </div><!--End of panel-->                              
        </div>  <!--End of inside tab content--> 
       </div>                     
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</section>

@endsection
