@extends('member.layout.auth')

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif
    <!--  section 1  -->
   <section>
      <div class="container topspacing">
        <div class="row">
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('view_popular') }}" class="thumbnail">
              <img src="{{ asset('img/popularmatches.png') }}" class="img-circle" alt="...">
            </a>
          </div>
          <!--<div class="col-xs-6 col-md-3">
            <a href="{{ route('onefive') }}" class="thumbnail">
              <img src="{{ asset('img/15.jpg') }}" class="img-circle" alt="...">
            </a>
          </div>-->
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('twofive') }}" class="thumbnail">
              <img src="{{ asset('img/25goals.png') }}" class="img-circle" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('double') }}" class="thumbnail">
              <img src="{{ asset('img/double.jpg') }}" class="img-circle" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('firsthalf') }}" class="thumbnail">
              <img src="{{ asset('img/firsthalf.jpg') }}" class="img-circle" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('sportpesa') }}" class="thumbnail">
              <img src="{{ asset('img/sportpesa.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('awaywin') }}" class="thumbnail">
              <img src="{{ asset('img/awaywin.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('homewin') }}" class="thumbnail">
              <img src="{{ asset('img/homewin.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('draw') }}" class="thumbnail">
              <img src="{{ asset('img/draw.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('correct') }}" class="thumbnail">
              <img src="{{ asset('img/correctscore.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('megajackport') }}" class="thumbnail">
              <img src="{{ asset('img/megajackport.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('goalgoal') }}" class="thumbnail">
              <img src="{{ asset('img/goalgoal.jpg') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
        </div>
      </div>
  </section> 
    <!--  end section 1  -->
@endsection
