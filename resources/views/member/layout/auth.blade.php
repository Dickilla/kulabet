<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kula-Bet , The worlds best soccer prediction site</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >

    <!-- Custom CSS -->
    <link href="{{ asset('css/logo-nav.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" >

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >
    <!-- Custom Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >

    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @include('member.layout.menu')

    @yield('content')

    <!-- Scripts -->
   <!--==============footer================-->
    
      <div class="container-fluid kifooter center-block marginles">
        <div class"row">
          <div class="col-lg-12 " align="center">
            <small>Copyright &copy; 2016 Kula-Bet | All Rights Reserved</small>
          </div>
        </div>
      </div>
<!--==============end footer================-->

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.js') }}"> </script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrapValidator.js') }}"></script>

    <!-- Custom js -->
     <script>
      $(document).ready(function(){
          $("#myBtn").click(function(){
              $("#myModal").modal();
          });
      });
      </script>
        <script type="text/javascript">
              $(document).ready(function() {
                $('#registerr').bootstrapValidator({
                  message: 'This value is not valid',
                  feedbackIcons: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                    firstname: {
                            message: 'The First name is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The First name is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The First name must be more than 2 and less than 30 characters long'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z]+$/,
                                    message: 'The First name can only consist of alphabetical'
                                }
                            }
                        },
                    lastname: {
                            message: 'The Last name is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The last name is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The last name must be more than 2 and less than 30 characters long'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z]+$/,
                                    message: 'The last name can only consist of alphabetical'
                                }
                            }
                        },
                    tel: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Phone Number'
                                },
                                digits: {
                                    message: 'The phone number is not valid'
                                },
                                stringLength: {
                                    min: 10,
                                    max: 12,
                                    message: 'Must be 10 digits'
                                }
                            }
                        },
                    email: {
                            validators: {
                                notEmpty: {
                                    message: 'Please Enter Your Email'
                                },
                                emailAddress: {
                                    message: 'Please Enter a valid Email'
                                }
                            }
                        },
                   password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            different: {
                                field: 'email',
                                message: 'The password cannot be your Email'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The password must have at least 8 characters'
                            }
                        }
                    },
                   password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password does not match'
                            },
                        }
                    },
                      }
                  });
              });
            </script>

</body>
</html>
