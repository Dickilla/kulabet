<nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('img/kulabet.png') }}" alt="holder"  class="img-responsive">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    date
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guard('member')->user())
                    <li>
                        <a class="btn btn-success btn-sm" href="{{ route('memberhome') }}"><i class="fa fa-home fa-1x"></i>&nbsp;home</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-success btn-sm" data-toggle="dropdown"><i class="fa fa-home fa-1x"></i>&nbsp;Actions <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="btn btn-success btn-sm" href="{{ route('view_popular') }}">Popular Games</a>
                            </li>
                            <li>
                                <a class="btn btn-success btn-sm" href="{{ route('view_won') }}">Won Tips</a>
                            </li>
                            <li>
                                <a class="btn btn-success btn-sm" href="{{ route('view_best') }}">Best Tips</a>
                            </li>
                        </ul>
                    </li>
                    <!--
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-home fa-1x"></i>&nbsp; Home</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-bar-chart fa-1x"></i>&nbsp; Live Scores</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-newspaper-o fa-1x"></i>&nbsp; News</a>
                    </li>-->
                    <li>
                        <a class="btn btn-success btn-sm" href="{{ route('contact') }}"><i class="fa fa-phone fa-1x"></i>&nbsp; Contact Us</a>
                    </li>
                        <li class="dropdown">
                            <a class="btn btn-success btn-sm" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               <span class="glyphicon glyphicon-user"></span> &nbsp;{{ Auth::guard('member')->user()->firstname }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('memberprofile', Auth::guard('member')->user()->id) }}"> Profile</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ url('/admin/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/member/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li><a class="btn btn-success btn-sm" href="{{ url('/member/login') }}">Login</a></li>
                        <li><a class="btn btn-success btn-sm" href="{{ url('/member/register') }}">Register</a></li>                        
                    @endif
                </ul>
            </div>
        </div>
    </nav>