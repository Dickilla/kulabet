@extends('admin.layout.auth')

@section('content')

<section >
    <div class="container topspacing">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><b>Message Deatails</b></h1>
			</div>
			<div class="col-md-3">
				
			</div>				   	
            <div class="col-md-6">
                <div class="thumbnail">
                    
                    <div class="caption">
                    	<div class="table-responsive-force">
                 				<table class="table">
                                  <tbody>
                                  	<tr>
                                  		<th>NAME:</th>
                                  		<th>{{ $msg->name }}</th>	
                                  	</tr> 
                                  	<tr>
                                  	    <th>EMAIL:</th>
                                  		<th>{{ $msg->email }}</th>	
                                  	</tr>
                                  	<tr>
                                  	    <th>TELEPHONE:</th>
                                  		<th>{{ $msg->tel }}</th>	
                                  	</tr>
                                  	<tr>
                                  		<th>MESSAGE:</th>	
                                  		<th>{{ $msg->message }}</th>
                                  	</tr>
                                  </tbody>
                                </table>
                 		</div>
                    </div>
                </div>
            </div>	
			<div class="col-md-3">

			</div>			  
		</div>
	</div>
</section>   

@endsection