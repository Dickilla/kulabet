<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kula-Bet , The worlds best soccer prediction site</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >

    <!-- Custom CSS -->
    <link href="{{ asset('css/logo-nav.css') }}" rel="stylesheet" type="text/css" >

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >
    <!-- Custom Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >

    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
</head>

<body>
      <!--========================= start of Navbar===========================-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="img/kulabet.png" alt="" class="img-responsive">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="btn btn-success btn-sm" href="{{ route('memberhome') }}"><i class="fa fa-home fa-1x"></i>home</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-soccer-ball-o fa-1x"></i>&nbsp; Popular Matches</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-money fa-1x"></i>&nbsp; Won Tips</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="{{ route('contact') }}"><i class="fa fa-phone fa-1x"></i>&nbsp; Contact Us</a>
                    </li>
                    <li><a class="btn btn-success btn-sm" href="{{ url('/member/login') }}">Login</a></li>
                    <li><a class="btn btn-success btn-sm" href="{{ url('/member/register') }}">Register</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!--========================= End of Navbar===========================-->
    <!--  section 1  -->
    <section>
        <div class="container">
            <div class="row topspacing">
                <div class="col-md-7">
                    <img src="img/pic.jpg" alt="Generic placeholder image" style="margin-right:15px;" height="300" width="650" class="img-responsive">
                    <h3 style="color:#FF0000;">Looking for Tips? </h3>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;Discover the world of sports investment and see how to increase your profits</h4>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;We deliver the best football predictions available</h4>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;Without data, your are just but another gambler</h4>
                    <h4><img src="img/tick.png" alt="Generic placeholder image" style="float:left;" height="20" width="20">&nbsp;Today's popular matches predictions</h4>
                </div>
                <div id="sidebar" class="col-md-5">
                    <div class="panel panel-success">
                        <div class="regheader">
                            <h3 class="panel-title" align="center"><b>REGISTER HERE</b></h3>
                        </div>
                        <div id="sidenav" class="panel-body regbody">
                            <form class="form-horizontal" id="register" role="form" method="POST" action="{{ url('/member/register') }}">
                                {{ csrf_field() }}
                              <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                <label for="firstname" class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>

                                    @if ($errors->has('firstname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('firstname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label for="lastname" class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" autofocus>

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('lastname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
                                <label for="tel" class="col-md-4 control-label">Phone Number</label>

                                <div class="col-md-6">
                                    <input id="tel" type="text" class="form-control" name="tel" value="{{ old('tel') }}" autofocus>

                                    @if ($errors->has('tel'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('tel') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                             <input id="active" value="0" type="hidden" class="form-control" name="active">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password_confirmation" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                    <a class="btn btn-success btn-sm" href="{{ url('/member/login') }}">Login</a>
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!--  end section 1  -->
    <!--  Section 2 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Today's Free Tips</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($free) >= 1)
                                      @foreach($free as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }} </a></td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-eye-open">&nbsp;VIEW</a>
                                          </td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Free Tips Today</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>  

    <!-- End section 2  -->
    <!--  Section 3 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Popular Matches</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Details</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($popular) >= 1)
                                      @foreach($popular as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('memreg') }}">{{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td> <a href="{{ route('memreg') }}" class="btn btn-success"><span class="glyphicon glyphicon-eye-open">&nbsp;VIEW</a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Popular Matches</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 3  -->
    <!--  Section 4 -->
    <section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Latest Won Results</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
                                  <thead>
                                    <tr>
                                      <th>League</th>
                                      <th>Teams</th>
                                      <th>Tip</th>
                                      <th>Results</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($won) >= 1)
                                      @foreach($won as $content)
                                      <tr>
                                          <td> {{ $content->league }}  </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}">{{ $content->teamone }} vs {{ $content->teamtwo }}</a> </td>
                                          <td> {{ $content->category }}  </td>
                                          <td> <a href="{{ route('outdetails', $content->gameID) }}" class="btn btn-success"><span class="glyphicon glyphicon-ok"></a></td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Latest Won Results</b></th>
                                      </tr>
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   

    <!-- End section 4  -->
    <!--==============footer================-->
    
      <div class="container-fluid kifooter center-block marginles">
        <div class"row">
          <div class="col-lg-12 " align="center">
            <small>Copyright &copy; 2016 Kula-Bet | <a href="{{ route('admlogin') }}">All Rights Reserved </small>
          </div>
        </div>
      </div>
<!--==============end footer================-->

    
    <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrapValidator.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#register').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            firstname: {
                    message: 'The First name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The First name is required and cannot be empty'
                        },
                        stringLength: {
                            min: 4,
                            max: 30,
                            message: 'The First name must be more than 2 and less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The First name can only consist of alphabetical'
                        }
                    }
                },
            lastname: {
                    message: 'The Last name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The last name is required and cannot be empty'
                        },
                        stringLength: {
                            min: 4,
                            max: 30,
                            message: 'The last name must be more than 2 and less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The last name can only consist of alphabetical'
                        }
                    }
                },
            tel: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter Phone Number'
                        },
                        digits: {
                            message: 'The phone number is not valid'
                        },
                        stringLength: {
                            min: 10,
                            max: 12,
                            message: 'Must be 10 digits'
                        }
                    }
                },
            email: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter Your Email'
                        },
                        emailAddress: {
                            message: 'Please Enter a valid Email'
                        }
                    }
                },
           password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'email',
                        message: 'The password cannot be your Email'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            },
           password_confirmation: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password does not match'
                    },
                }
            },
              }
          });
      });
    </script>


</body>

</html>