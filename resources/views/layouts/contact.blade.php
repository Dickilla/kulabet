@extends('member.layout.auth')

@section('content')

<section >
     <div class="container topspacing">
      @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
    @endif
          <div class="row">                
              <div class="col-md-4">
                  <h3>Contact Details</h3>
                  <p>
                      Kula Bet<br>
                  </p>
                  <p><i class="fa fa-phone"></i> 
                      <abbr title="Phone">P</abbr>: 0713609450 (sms only)</p>
                  <p><i class="fa fa-envelope-o"></i> 
                      <abbr title="Email">E</abbr>: <a href="mailto:info@kulabet.com">info@kulabet.com</a>
                  </p>
                  <ul class="list-unstyled list-inline list-social-icons">
                      <li>
                          <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                      </li>
                  </ul>
              </div>
              <div class="col-md-8">
                  <h3>Send us a Message</h3>
                  <form  role="form" method="POST" action="{{ url('messagestore') }}">
                        {{ csrf_field() }}
                      <div class="control-group form-group">
                          <div class="controls">
                              <label>Full Name:</label>
                              <input type="text" class="form-control" name="name" value="{{ old('name') }}" >
                              @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                          </div>
                      </div>
                      <div class="control-group form-group">
                          <div class="controls">
                              <label>Phone Number:</label>
                              <input type="tel" class="form-control" name="tel" value="{{ old('tel') }}">
                              @if ($errors->has('tel'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tel') }}</strong>
                                    </span>
                                @endif
                          </div>
                      </div>
                      <div class="control-group form-group">
                          <div class="controls">
                              <label>Email Address:</label>
                              <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                              @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                          </div>
                      </div>
                      <div class="control-group form-group">
                          <div class="controls">
                              <label>Message:</label>
                              <textarea rows="5" cols="20" class="form-control" name="message" value="{{ old('message') }}" ></textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary">Send Message</button>
                  </form>
              </div>              
          </div>
      </div>
</section>   


@endsection