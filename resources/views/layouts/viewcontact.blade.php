<?php
    use Illuminate\Support\Facades\DB;
?>
@extends('admin.layout.auth')

@section('content')

<section >
     <div class="container topspacing">
          <div class="row">
             <div id="sidebar" class="col-md-1">
                
            </div>
                
            <div class="col-md-10">
              
             <div id="welcome" class="panel panel-success">
               <div class="panel-heading">
                  <h2 class="panel-title text-center"><b>Messages</b></h2>
               </div>
               <div class="panel-body">
                 <div class="table-responsive-force">
                 <table class="table table-bordered table-hover table-striped tablesorter">
                                  <thead>
                                    <tr>
                                      <th>Name</th>
                                      <th>Email</th>
                                      <th colspa="2">Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @if (count($msg) >= 1)
                                      @foreach($msg as $msgs)
                                      <tr>
                                          <td> {{ $msgs->name }}  </td>
                                          <td> {{ $msgs->email }}  </td>
                                          <td> <a href="{{ route('read', $msgs->id) }}" class="btn btn-success">READ<span class="label label-primary">
                                                @php                                                                            
                                                    $records = DB::table('message')->where([['id', $msgs->id],['status',1],])->first();
                                                @endphp
                                            @if (count($records) === 0)  
                                              new                  
                                            @else
                                                  old
                                            @endif
                                            </span></a>
                                          </td>                                          
                                          <td>
                                            <form role="form" method="post" action="deletemsg/{{ $msgs->id }}" class="form-horizontal">
                                              {{ csrf_field() }}
                                              <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-trash "></span><b>Delete</b></button>
                                            </form>
                                          </td>
                                      </tr>
                                      @endforeach
                                    @else
                                      <tr>                                        
                                        <th class="text-center" colspan="4"><b>No Messages</b></th>
                                      </tr> 
                                    @endif
                                    
                                  </tbody>
                                </table>
                 </div>                     
               </div>
               </div>
            
               
              </div>
              <div id="sidebar" class="col-md-1">
                
            </div>
              
             </div>
        </div>
    </section>   


@endsection