<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kula-Bet,The worlds best soccer prediction site</title>

    <!-- Bootstrap Core CSS
    <link href="css/bootstrap.min.css" rel="stylesheet">-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >

    <!-- Custom CSS
    <link href="css/logo-nav.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">-->
    <link href="{{ asset('css/logo-nav.css') }}" rel="stylesheet" type="text/css" >

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
</head>

<body>
      <!--========================= start of Navbar===========================-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="img/kulabet.png" alt="" class="img-responsive">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-home fa-1x"></i>&nbsp; Home</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-soccer-ball-o fa-1x"></i>&nbsp; Popular Matches</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-money fa-1x"></i>&nbsp; Won Tips</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-bar-chart fa-1x"></i>&nbsp; Live Scores</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-newspaper-o fa-1x"></i>&nbsp; News</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-phone fa-1x"></i>&nbsp; Contact Us</a>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" href="#"><i class="fa fa-sign-in fa-1x"></i>&nbsp; Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!--========================= End of Navbar===========================-->
    <!--  section 1  -->
   <section>
      <div class="container topspacing">
        <div class="row">
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('view_popular') }}" class="thumbnail">
              <img src="img/popularmatches.png" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/15.jpg" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/25goals.png" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/double.jpg" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/firsthalf.jpg" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/sportpesa.jpg" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/super.jpg" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="img/betin.jpg" alt="...">
            </a>
          </div>
        </div>
      </div>
  </section>
    <!--  end section 1  -->
    <!--==============footer================-->

      <div class="container-fluid kifooter center-block marginles">
        <div class"row">
          <div class="col-lg-12 " align="center">
            <small>Copyright &copy; 2016 Kula-Bet | All Rights Reserved</small>
          </div>
        </div>
      </div>
<!--==============end footer================-->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom js -->
    <script src="js/megamenu.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
