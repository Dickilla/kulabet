@extends('admin.layout.auth')

@section('content')
<?php
	use Carbon\Carbon;
?>
<section class="admin" id="admin">
     <div class="container topspacing">
		  <div class="row">
			 <div id="sidebar" class="col-md-1">
				
			</div>
				
			<div class="col-md-10">
			  
	         <div id="welcome" class="panel panel-success">
			   <div class="panel-heading">
				  <h3 class="panel-title"><b>Members</b></h3>
			   </div>
			   <div class="panel-body">
			     <div class="table-responsive-force">
			     <table class="table table-bordered table-hover table-striped tablesorter">
								  <thead>
									<tr>
									  <th>FirstName</th>
									  <th>lastName</th>
									  <th>Phone Number</th>
									  <th>Email</th>
									  <th>Status</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	
									@foreach($members as $member)
										@php					
											$action = $member->active;
											$now = Carbon::now();
											$current = Carbon::now();
											$created_at = new Carbon($member->created_at);
										    $Expire = $current->addDays(30);
										    $difference = ($Expire->diff($current)->days < 0)
										    ? 'today'
										    : $created_at->diffForHumans($current);

										@endphp
									<tr>
										<td> {{ $member->firstname }}  </td>
										<td> {{ $member->lastname }}  </td>
										<td> {{ $member->tel }}  </td>
										<td> {{ $member->email }}  </td>
										<td> {{ $difference }} </td>
										<td>
				                            @if($action == 0)
				                            	<a href="{{ route('acti_vate', $member->id) }}" 
				                             class="btn btn-primary" onclick="return confirm('Activate <?php echo $member->firstname; ?>');"><span class="glyphicon glyphicon-ok-circle"></span> Activate </a>
				                            				                           
				                            @else	 
				                            	<a href="{{ route('deacti_vate', $member->id) }}" 
				                             class="btn btn-danger" onclick="return confirm('Deactivate <?php echo $member->firstname; ?>');"><span class="glyphicon glyphicon-remove-circle"></span> Deactivate </a>
											@endif
				                          
				                        </td>										
									</tr>
									@endforeach
									
								  </tbody>
								</table>
				 </div>						
			   </div>
			   </div>
			
			   
			  </div>
			  <div id="sidebar" class="col-md-1">
				
			</div>
			  
			 </div>
		</div>
	</section>   


@endsection