@extends('admin.layout.auth')

@section('content')

<section >
    <div class="container topspacing">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><b>{{ $admin->firstname }} &nbsp;{{ $admin->lastname }}</b></h1>
			</div>
			<div class="col-md-3">
				
			</div>				   	
            <div class="col-md-6">
                <div class="thumbnail">
                    
                    <div class="caption">
                    	<div class="table-responsive-force">
                 				<table class="table table-condensed ">
                                  <tbody> 
                                  	<tr>
                                  	    <th>Email:</th>
                                  		<td>{{ $admin->email }}</td>	
                                  	</tr>
                                  	<tr>
                                  	    <th>Mobile Number:</th>
                                  		<td>{{ $admin->tel }}</td>	
                                  	</tr>
                                  	<tr>
                                  	    <th>Subscription Ends:</th>	
                                  		<td>{{ $admin->tel }}</td>
                                  	</tr>
                                    <tr>
                                      <td></td>
                                        <th><a class="btn btn-success btn-sm" href="{{ route('editadmin', Auth::guard('admin')->user()->id) }}"> <span class="glyphicon glyphicon-edit ">&nbsp;Edit profile</a></th> 
                                        
                                    </tr>


                                  </tbody>
                                </table>
                 		</div>
                    </div>
                </div>
            </div>	
			<div class="col-md-3">

			</div>			  
		</div>
	</div>
</section>   

@endsection