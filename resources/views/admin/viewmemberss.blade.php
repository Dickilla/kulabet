@extends('admin.layout.auth')

@section('content')
<?php
	use Carbon\Carbon;
?>
<section class="admin" id="admin">
     <div class="container topspacing">
		  <div class="row">
			 <div id="sidebar" class="col-md-1">
				
			</div>
				
			<div class="col-md-10">
			  
	         <div id="welcome" class="panel panel-success">
			   <div class="panel-heading">
				  <h3 class="panel-title text-center"><b>Members</b></h3>
			   </div>
			   <div class="panel-body">
			     <div class="table-responsive-force">
			     <table class="table table-bordered table-hover table-striped tablesorter viewmember">
								  <thead>
									<tr>
									  <th>FirstName</th>
									  <th>lastName</th>
									  <th>Phone Number</th>
									  <th>Email</th>
									  <th>Status</th>
									  <th>Status</th>
									  <th>Action</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
								  	
									@foreach($members as $member)
										@php					
											$current = Carbon::now();
											$created_at = new Carbon($member->timespan);
										    $Expire = $current->addDays(0);
										    $difference = Carbon::now()->subMinutes(2)->diffForHumans();
										    $difference = ($Expire->diff($current)->days < 0)
										    ? 'today'
										    : $created_at->diffForHumans($current);


										@endphp
									<tr>
										<td> {{ $member->firstname }}  </td>
										<td> {{ $member->lastname }}  </td>
										<td> {{ $member->tel }}  </td>
										<td> {{ $member->email }}  </td>
										<td> {{ $difference }} </td>
										<td>
				                            @if($member->active == 0)
				                            	<p>Inctive</p>			                           
				                            @else
				                            	<p>Active</p>
				                           	@endif
				                          
				                        </td>
										<td>
				                            @if($member->active == 0)
				                            	<a href="{{ route('acti_vate', $member->id) }}" 
				                             class="btn btn-success" onclick="return confirm('Activate <?php echo $member->firstname; ?>');"><span class="glyphicon glyphicon-ok-circle"></span> Activate </a>
				                            				                           
				                            @else	 
				                            	<a href="{{ route('deacti_vate', $member->id) }}" 
				                             class="btn btn-primary" onclick="return confirm('Deactivate <?php echo $member->firstname; ?>');"><span class="glyphicon glyphicon-remove-circle"></span> Deactivate </a>
											@endif
				                          
				                        </td>
										<td>
										<form role="form" method="post" action="deletemember/{{ $member->id }}" class="form-horizontal">
											{{ csrf_field() }}
											<button type="submit" class="btn btn-danger" onclick="return confirm('Delete <?php echo $member->firstname; ?>');"><span class="glyphicon glyphicon-trash "></span><b>Delete</b></button>
										</form>
										</td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>
				 </div>						
			   </div>
			   </div>
			
			   
			  </div>
			  <div id="sidebar" class="col-md-1">
				
			</div>
			  
			 </div>
		</div>
	</section>   


@endsection