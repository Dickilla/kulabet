@extends('admin.layout.auth')

@section('content')

<section class="admin" id="admin">
     <div class="container topspacing">
		  <div class="row">
			 <div id="sidebar" class="col-md-1">
				
			</div>
				
			<div class="col-md-10">
			  
	         <div id="welcome" class="panel panel-success">
			   <div class="panel-heading">
				  <h3 class="panel-title text-center"><b>Activate Won and best games</b></h3>
			   </div>
			   <div class="panel-body">
			     <div class="table-responsive-force">
			     <table class="table table-bordered table-hover table-striped tablesorter wonbest">
								  <thead>
									<tr>
									  <th>Country</th>
									  <th>League</th>
									  <th>Teams</th>
									  <th>Won</th>
									  <th>Best</th>
									</tr>
								  </thead>
								  <tbody>
								  	
									@foreach($contents as $content)
									<tr>
										<td> {{ $content->country }}  </td>
										<td> {{ $content->league }}  </td>
										<td> {{ $content->teamone }} vs {{ $content->teamtwo }} </td>
										<td>
				                            @if($content->won == 0)
				                            	<a href="{{ route('acti_won', $content->gameID) }}" 
				                             class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Activate </a>
				                            				                           
				                            @else	 
				                            	<a href="{{ route('deacti_won', $content->gameID) }}" 
				                             class="btn btn-primary"><span class="glyphicon glyphicon-remove-circle"></span> Deactivate </a>
											@endif				                          
				                        </td>
										<td>
				                            @if($content->best == 0)
				                            	<a href="{{ route('acti_best', $content->gameID) }}" 
				                             class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Activate </a>
				                            				                           
				                            @else	 
				                            	<a href="{{ route('deacti_best', $content->gameID) }}" 
				                             class="btn btn-primary"><span class="glyphicon glyphicon-remove-circle"></span> Deactivate </a>
											@endif				                          
				                        </td>
									</tr>
									@endforeach
									
								  </tbody>
								</table>
				 </div>						
			   </div>
			   </div>
			
			   
			  </div>
			  <div id="sidebar" class="col-md-1">
				
			</div>
			  
			 </div>
		</div>
	</section>   


@endsection