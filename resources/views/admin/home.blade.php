@extends('admin.layout.auth')

@section('content')
<section>
      <div class="container topspacing">
        <div class="row">
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('view_members') }}" class="thumbnail">
              <img src="{{ asset('img/activatemember.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('new_cont') }}" class="thumbnail">
              <img src="{{ asset('img/addgame.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('view_cont') }}" class="thumbnail">
              <img src="{{ asset('img/viewgames.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('add_tip') }}" class="thumbnail">
              <img src="{{ asset('img/addtip.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('view_tip') }}" class="thumbnail">
              <img src="{{ asset('img/viewtip.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('freepopular') }}" class="thumbnail">
              <img src="{{ asset('img/freeandpopular.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('wonbest') }}" class="thumbnail">
              <img src="{{ asset('img/wontips.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
          <div class="col-xs-6 col-md-3">
            <a href="{{ route('wonbest') }}" class="thumbnail">
              <img src="{{ asset('img/besttips.png') }}" class="img-circle img-responsive" alt="...">
            </a>
          </div>
        </div>
      </div>
  </section> 
@endsection
