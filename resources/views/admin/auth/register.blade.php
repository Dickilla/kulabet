@extends('admin.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="regheader">
                    <h3 class="panel-title" align="center"><b>Admin Register here</b></h3>
                </div>
                <div class="panel-body">

                    @include('partials.flash')
                    @include('partials.alerts.errors')
                    <form id="registerr" method="POST" action="{{url('/admin/register')}}">
                        {{ csrf_field()}}
                        <div class="form-group">
                            <label>First Name</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input class="form-control" id="firstname" type="text" name="firstname" placeholder="First Name" value="{{ old('firstname') }}" required autofocus >
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input class="form-control" id="firstname" type="text" name="lastname" placeholder="Last Name" value="{{ old('lastname') }}" required autofocus >
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Mobile Number</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                    <input class="form-control" id="tel" type="tel" name="tel" placeholder="(254)" value="{{ old('tel') }}" required autofocus >
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>E-Mail Address</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input name="email"  id="email" placeholder="E-Mail Address" class="form-control"  type="email" value="{{ old('email') }}" required autofocus >
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input class="form-control" id="password" type="password" name="password" placeholder="Password" required autofocus>
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password:</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input class="form-control" id="password_confirmation" type="password" name="password_confirmation" placeholder="Confirm Password" required autofocus>
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>


                        <div class="form-group{{ $errors->has('termcondition') ? ' has-error' : '' }}">
                            <div class="controls">
                                <input type="checkbox" id="brand1" name="termcondition"  value="termcondition"  autofocus>
                                <label for="brand1"><span>&nbsp;</span>I agree to the T&C. Read <a href="">here </a></label>
                            </div>
                        </div>
                        <hr class="colorgraph">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <button type="submit" class="btn btn-success btn-block"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</button>
                        </div>
                        <div class="pull-right">
                            <p>Already a member.<a href="{{ url('/admin/login') }}">&nbsp;<i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Login here</a></p>
                        </div>
                        <div class="clearfix"> </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
