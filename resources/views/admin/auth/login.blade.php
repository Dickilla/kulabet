@extends('admin.layout.auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="regheader">
                    <h3 class="panel-title" align="center"><b>Admin Login</b></h3>
                </div>
                <div class="panel-body regbody">
                    @include('partials.flash')
                    @include('partials.alerts.errors')
                    <form id="login" role="form" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field()}}
                        <div class="form-group">
                            <label>E-Mail Address</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input class="form-control" id="email" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus >
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <div class="inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input class="form-control" id="password" type="password" name="password" placeholder="Password" required autofocus>
                                </div>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <span class="button-checkbox">
								<input type="checkbox" id="remember_me" name="remember_me"  >
								<label for="remember_me"><span>&nbsp;</span>Remember Me</label>
							<a href="{{ url('/admin/password/reset') }}" class="btn btn-link pull-right"><i class="fa fa-key" aria-hidden="true"></i> Forgot Password</a>
						</span>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn  btn-success btn-block" onclick="validation();"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</button>
                            </div>
                        <!-- <div class="col-xs-6 col-sm-6 col-md-6">
								<a href="{{url('/system/register')}}" class="btn  btn-success btn-block"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a>
							</div> --><br/>
                            <div class="pull-right">
                                <p>Not registered?<a href="{{ url('/admin/register') }}">&nbsp;<i class="fa fa-user-plus " aria-hidden="true"></i>&nbsp;Register here</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
