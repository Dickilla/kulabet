<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kula-Bet , The worlds best soccer prediction site</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >

    <!-- Custom CSS -->
    <link href="{{ asset('css/logo-nav.css') }}" rel="stylesheet" type="text/css" > 
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" >

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >
    <!-- Custom Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >

    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
    
    <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"> </script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/bootstrapValidator.js') }}"></script>
    <script src="{{ asset('js/vue.js') }}"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @include('admin.layout.menu')

    @yield('content')

    <!-- Scripts -->
   <!--==============footer================-->
    
      <div class="container-fluid kifooter center-block marginles">
        <div class"row">
          <div class="col-lg-12 " align="center">
            <small>Copyright &copy; 2016 Kula-Bet | All Rights Reserved</small>
          </div>
        </div>
      </div>
<!--==============end footer================-->



    <script type="text/javascript">
        $('.viewmember').DataTable({
            select:true,
        });

        $('.freepop').DataTable({
            select:true,
        });

        $('.wonbest').DataTable({
            select:true,
        });
        
        $('.viewcont').DataTable({
            select:true,
        });
        
        $('.viewtip').DataTable({
            select:true,
        });
    </script>
        <script type="text/javascript">
              $(document).ready(function() {
                $('#addcont').bootstrapValidator({
                  message: 'This value is not valid',
                  feedbackIcons: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                    country: {
                            message: 'The country is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The country is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The country must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    league: {
                            message: 'The league is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The league is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The league must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    teamone: {
                            message: 'The teamone is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The teamone is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The teamone must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    teamtwo: {
                            message: 'The teamtwo is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The teamtwo is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The teamtwo must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    category: {
                            message: 'The category is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The category is required and cannot be empty'
                                },
                            }
                        },
                      }
                  });
              });
            </script>
        <script type="text/javascript">
              $(document).ready(function() {
                $('#addtip').bootstrapValidator({
                  message: 'This value is not valid',
                  feedbackIcons: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                    cat_name: {
                            message: 'The cat_name is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The cat_name is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The cat_name must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    cat_alias: {
                            message: 'The cat_alias is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The cat_alias is required and cannot be empty'
                                },
                            }
                        },
                      }
                  });
              });
            </script>

    <script type="text/javascript">
    </script>

</body>
</html>
