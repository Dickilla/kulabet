<?php
    use Illuminate\Support\Facades\DB;
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('img/kulabet.png') }}" alt="holder"  class="img-responsive">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guard('admin')->user())
                        <li>
                            <a class="btn btn-success btn-sm" href="{{ route('adminhome') }}"><i class="fa fa-home fa-1x"></i>&nbsp; Home</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle btn btn-success btn-sm" data-toggle="dropdown"><i class="fa fa-home fa-1x"></i>&nbsp;Actions <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('freepopular') }}">Free Tips</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('freepopular') }}">Popular Games</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('wonbest') }}">Won Tips</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('wonbest') }}">Best Tips</a>
                                </li>
                            </ul>
                        </li>
                       <li>
                            <a class="btn btn-success btn-sm" href="{{ route('view_members') }}"><i class="fa fa-soccer-ball-o fa-1x"></i>&nbsp; Activate Members</a>
                        </li>
                       <li class="dropdown">
                            <a href="{{ route('new_cont') }}" class="dropdown-toggle btn btn-success btn-sm" data-toggle="dropdown"><i class="fa fa-home fa-1x"></i>&nbsp; Games <b class="caret"></b></a>                    
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('new_cont') }}">Add Games</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('view_cont') }}">View Games</a>
                                </li>
                            </ul>
                        </li>
                       <li class="dropdown">
                            <a href="{{ route('add_tip') }}" class="dropdown-toggle btn btn-success btn-sm" data-toggle="dropdown"><i class="fa fa-home fa-1x"></i>&nbsp; Tips <b class="caret"></b></a>                    
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('add_tip') }}">Add Tips</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('view_tip') }}">View Tips</a>
                                </li>
                            </ul>
                        </li>
                        <!--<li>
                            <a class="btn btn-success btn-sm" href="{{ route('view_cont') }}"><i class="fa fa-bar-chart fa-1x"></i>&nbsp; View Matches</a>
                        </li>-->
                        <li>
                            <a class="btn btn-success btn-sm" href="{{ route('view_message') }}"> 
                                <span class="label label-primary">
                                   @php                                                                            
                                        $contents = DB::table('message')->where('status', '=', 0)->count();
                                    @endphp
                                        {{ $contents }}
                                  </span><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></a>
                        </li>
                        <li class="dropdown">
                            <a class="btn btn-success btn-sm" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="glyphicon glyphicon-user"></span> &nbsp;{{ Auth::guard('admin')->user()->firstname }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ route('adminprofile', Auth::guard('admin')->user()->id) }}"> Profile</a>
                                </li>
                                <li>
                                    <a class="btn btn-success btn-sm" href="{{ url('/admin/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li><a class="btn btn-success btn-sm" href="{{ url('/admin/login') }}">Login</a></li>
                        <li><a class="btn btn-success btn-sm" href="{{ url('/admin/register') }}">Register</a></li>                      
                    @endif
                </ul>
            </div>
        </div>
    </nav>