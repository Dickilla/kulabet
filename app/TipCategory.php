<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipCategory extends Model
{    
    protected $table = 'category';

    protected $fillable = [
        'cat_name', 'cat_alias',
    ];

}
