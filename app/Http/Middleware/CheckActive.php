<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'member')
    {
        if (Auth::guard($guard)->check() && Auth::guard('member')->user()->active == 1) {
            return $next($request);
        }else{
        return redirect('index');
        }
    }
}
