<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CheckIfExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */    
    public function handle($request, Closure $next, $guard = 'member')
    {
        if (Auth::guard($guard)->check() && Auth::guard('member')->user()->active == 1) {
            if(Auth::guard('member')->user()->timespan < Carbon::now() ){
                return redirect()->route('autodeactivate', ['id' => Auth::guard('member')->user()->id]);
            }else{
                return $next($request);
            }
            
        }else{
        return redirect('index');
        }
    }
}
