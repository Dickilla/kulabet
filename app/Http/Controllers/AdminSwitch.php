<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Contents;

class AdminSwitch extends Controller
{    
    public function freepopular(){
        $contents = DB::table('contents')->get();
        return view('admin.freepopular', ['contents' => $contents]);

    }

    public function actfreetip($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->freetip = "1";

        $act->save();

        return redirect()->route('freepopular');
    }

    public function deactfreetip($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->freetip = "0";

        $act->save();

        return redirect()->route('freepopular');
    }

    public function actpopular($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->popular = "1";

        $act->save();

        return redirect()->route('freepopular');
    }

    public function deactpopular($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->popular = "0";

        $act->save();

        return redirect()->route('freepopular');
    }

    public function wonbest(){
        $contents = DB::table('contents')->get();
        return view('admin.wonbest', ['contents' => $contents]);

    }

    public function actwon($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->won = "1";

        $act->save();

        return redirect()->route('wonbest');
    }

    public function deactwon($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->won = "0";

        $act->save();

        return redirect()->route('wonbest');
    }

    public function actbest($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->best = "1";

        $act->save();

        return redirect()->route('wonbest');
    }

    public function deactbest($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->best = "0";

        $act->save();

        return redirect()->route('wonbest');
    }
}
