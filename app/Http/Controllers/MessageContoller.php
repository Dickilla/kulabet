<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Message;

use App\Http\Requests;

use App\Admin;

use App\Member;

use App\Contents;

use App\Alltime;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;

use Carbon\Carbon;

class MessageContoller extends Controller
{


    public function contact(){
        return view('layouts.contact');
    }

    public function messagestore(Request $request)
    {
        $this->validate($request, [
                    'name' => 'required',
                    'tel' => 'get_required_files()',
                    'email' => 'required',
                    ]);
        $msg = new Message;
        $msg->name = $request->name;
        $msg->tel = $request->tel;
        $msg->email = $request->email;
        $msg->message = $request->message;
        $msg->status = "0";
        $msg->save();
        Session::flash('flash_message', 'You successfully sent your message!');
        return redirect()->route('contact');
    }

    public function viewmessage(){
        $msg = DB::table('message')->latest()->get();
        return view('layouts.viewcontact', compact('msg'));
    }

    public function read($id){
        $msg = Message::findOrFail($id);
        $msg->status = "1";
        $msg->save();
        return view('layouts.read', compact('msg'));
    }

    public function deletemsg($id)
    {
        $msg = Message::findOrFail($id);
        $msg->delete();
        return redirect()->route('view_message');
    }
}
