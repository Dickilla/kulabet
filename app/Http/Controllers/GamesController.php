<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Member;

use App\Contents;

use App\TipCategory;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;

class GamesController extends Controller 
{
    public function onefive(){
        $contents = DB::table('contents')->where([['category', 'like', '%1.5%'],['hide',0],])->latest()->get();

        return view('games.15', ['contents' => $contents]);
    }

    public function twofive(){
        $contents = DB::table('contents')->where([['category', 'like', '%2.5%'],['hide',0],])->latest()->get();
        return view('games.25', ['contents' => $contents]);
    }

    public function double(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', 'like', '%double%'],['hide',0],])
            ->get();
        return view('games.double', ['contents' => $contents]);
    }

    public function firsthalf(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', '=', 'half time full time'],['hide',0],])
            ->get();
        return view('games.firsthalf', ['contents' => $contents]);
    }

    public function sportpesa(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', '=', 'sportpesa'],['hide',0],])
            ->get();
        return view('games.sportpesa', ['contents' => $contents]);
    }

    public function awaywin(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', 'like', '%away%'],['hide',0],])
            ->get();
        return view('games.awaywin', ['contents' => $contents]);
    }

    public function homewin(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', 'like', '%home%'],['hide',0],])
            ->get();
        return view('games.homewin', ['contents' => $contents]);
    }

    public function draw(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', 'like', '%draw%'],['hide',0],])
            ->get();
        return view('games.draw', ['contents' => $contents]);
    }

    public function correct(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', 'like', '%correct%'],['hide',0],])
            ->get();
        return view('games.correct', ['contents' => $contents]);
    }

    public function megajackport(){
        $contents = DB::table('contents')
            ->join('category', 'contents.category', '=', 'category.cat_alias')
            ->select('contents.category', 'category.cat_name','contents.country','contents.league','contents.teamone','contents.teamtwo','contents.gameID')
            ->where([['category.cat_name', '=', 'megajackport'],['hide',0],])
            ->get();
        return view('games.megajackport', ['contents' => $contents]);
    }

    public function goalgoal(){
        $contents = DB::table('contents')->where([['category', 'like', '%GG%'],['hide',0],])->latest()->get();
        return view('games.goalgoal', ['contents' => $contents]);
    }

    public function hide($id)
    {
        $act = Contents::findOrFail($id);

        $act->hide = "0";

        $act->save();

        return redirect()->route('view_cont');
    }

    public function unhide($gameID)
    {
        $act = Contents::findOrFail($gameID);

        $act->hide = "1";

        $act->save();

        return redirect()->route('view_cont');
    }

}
