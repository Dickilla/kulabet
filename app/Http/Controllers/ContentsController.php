<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Contents;

use App\TipCategory;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;

class ContentsController extends Controller
{
    public function newcont(){
        $category = DB::table('category')->latest()->get();
	    return view('contents.addcont', compact('category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                    'country' => 'required',
                    'league' => 'required',
                    'teamone' => 'required',
                    'teamtwo' => 'required',
                    ]);

    	$game = new Contents;
    	$game->country = $request->country;
    	$game->league = $request->league;
    	$game->teamone = $request->teamone;
    	$game->teamtwo = $request->teamtwo;
    	$game->category = $request->category;
    	$game->outcome = "none";
        $game->freetip = "0";
        $game->popular = "0";
        $game->best = "0";
        $game->won = "0";
        $game->hide = "0";
    	$game->save();

        Session::flash('flash_message', 'The Game have been successfully added!');
        return redirect()->route('view_cont');
    }

    public function viewcont(){
    	$contents = DB::table('contents')->latest()->get();
    	return view('contents.viewcont', ['contents' => $contents]);

    }

    public function home(){
        $free = DB::table('contents')->where([['freetip', 1],['hide',0],])->latest()->get();
        $popular = DB::table('contents')->where([['popular', 1],['hide',0],])->latest()->get();
        $won = DB::table('contents')->where([['won', 1],])->latest()->get();

        return view('layouts.home',compact('free','popular','won') );

    }

    public function details($gameID){
        $content = Contents::findOrFail($gameID);
        $cat = DB::table('category')->where('cat_alias', '=', $content->category)->first();

        return view('contents.viewdetails', compact('content','cat'));

    }

    public function outdetails($gameID){
        $content = Contents::findOrFail($gameID);
        $cat = DB::table('category')->where('cat_alias', '=', $content->category)->first();

        return view('contents.outviewdetails', compact('content','cat'));

    }

    public function editcont($gameID)
	{
	    $content = Contents::findOrFail($gameID);
        $category = DB::table('category')->get();

	    return view('contents.editcont', compact('content','category'));
	}

	public function update($gameID, Request $request)
	{
	    $content = Contents::findOrFail($gameID);

	    $input = $request->all();

	    $content->fill($input)->save();

        Session::flash('flash_message', 'The Game have been successfully updated!');
	    return redirect()->route('view_cont');
	}

	public function delete($gameID)
	{
	    $content = Contents::findOrFail($gameID);

	    $content->delete();

	    return redirect()->route('view_cont');
	}

    public function addtip(){
        return view('contents.addtip');
    }

    public function storetip(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required',
            'cat_alias' => 'required',
            ]);
        $tip = new TipCategory;
        $tip->cat_name = $request->cat_name;
        $tip->cat_alias = $request->cat_alias;
        $tip->save();

        Session::flash('flash_message', 'The Tip have been successfully added!');
        return redirect()->route('view_tip');
    }

    public function viewtip(){
        $tip = DB::table('category')->latest()->get();
        return view('contents.viewtip', compact('tip'));

    }

    public function deletetip($gameID)
    {
        $tip = TipCategory::findOrFail($gameID);

        $tip->delete();

        Session::flash('flash_message', 'The Tip have been successfully removed!');
        return redirect()->route('view_tip');
    }
}
