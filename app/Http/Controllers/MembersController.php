<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Member;

use App\Contents;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Session;

class MembersController extends Controller
{
    public function view(){
        $contents = DB::table('contents')->latest()->get();
        return view('members.viewgames', ['contents' => $contents]);

    }
    public function newmember(){
        return view('members.newmember');
    }
    public function memberhome(){
        return view('member.home');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'tel' => 'required',
                    'email' => 'required',
                    'password' => 'required',
                    'password-confirm' => 'required',
                    ]);
        $admin = new Member;
        $admin->firstname = $request->firstname;
        $admin->lastname = $request->lastname;
        $admin->tel = $request->tel;
        $admin->email = $request->email;
        $admin->password = md5($request->password);
        $admin->active = "0";
        $admin->save();
        Session::flash('flash_message', 'You have successfully Registered.Please proceed to payment to be activated!');
        return redirect()->route('member_login');
    }

    public function memberlogin(){
        return view('members.login');
    }

    public function memberlogout(){
        session_start();
        session_destroy();
        return view('members.login');
    }

    public function check(Request $request)
    {
        $username_log = $request->email;
        $password_log = md5($request->password);


        $admin = DB::table('members')->where('email', '=', $username_log)->first();
        $username_db = $admin->email;
        $password_db = $admin->password;
        $active = $admin->active;


        if ($password_db == $password_log && $username_log == $username_db && $active==1){

            session_start();
            $_SESSION["username"] = $username_log;
            return redirect()->route('view_game');
        }
        else{
            return("False");
        }
    }

    public function viewgame(){
        $contents = DB::table('contents')->latest()->get();
        return view('members.viewgames', ['contents' => $contents]);

    }

    public function viewtips(){
        $contents = DB::table('contents')->where([['freetip', 1],['hide',0],])->latest()->get();
        return view('members.freetip', ['contents' => $contents]);
    }

    public function viewpopular(){
        $contents = DB::table('contents')->where([['popular', 1],['hide',0],])->latest()->get();
        return view('members.popular', ['contents' => $contents]);

    }

    public function viewbest(){
        $contents = DB::table('contents')->where([['best', 1],['hide',0],])->latest()->get();
        return view('members.best', ['contents' => $contents]);

    }

    public function viewwon(){
        $contents = DB::table('contents')->where([['won', 1],])->latest()->get();
        return view('members.won', ['contents' => $contents]);

    }

    public function index(){
        $free = DB::table('contents')->where([['freetip', 1],['hide',0],])->latest()->get();
        $popular = DB::table('contents')->where([['popular', 1],['hide',0],])->latest()->get();
        $won = DB::table('contents')->where([['won', 1],])->latest()->get();

        return view('members.index',compact('free','popular','won') );

    }

    public function memberprofile($id)
    {
        $member = DB::table('members')->where('id', '=', $id)->first();
        $current = Carbon::now();
        $dx = new Carbon($member->timespan);
        $dt = $dx->toDayDateTimeString();
        $dy = $dx->diffInDays($current);

        return view('member.profile',compact('member','dt','dy','current','dx'));
    }

    public function memberupdate($id, Request $request)
    {
        $member = Member::findOrFail($id);

        $input = $request->all();

        $member->fill($input)->save();

        Session::flash('flash_message', 'Your profile have been successfully updated!');
        return redirect()->route('memberprofile',$id);
    }

    public function autodeactivate($id)
    {
        $act = Member::findOrFail($id);

        $act->active = "0";

        $act->save();

        return redirect()->route('index');
    }
}
