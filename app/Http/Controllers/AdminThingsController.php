<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Admin;

use App\Member;

use App\Contents;

use App\Alltime;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AdminThingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    public function adminhome(){
        return view('admin.home');
    }
    

    public function newadmin(){
		return view('admin.newadmin');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'tel' => 'required',
                    'email' => 'required',
                    'password' => 'required',
                    'password-confirm' => 'required',
                    ]);
    	$admin = new Admin;
    	$admin->firstname = $request->firstname;
    	$admin->lastname = $request->lastname;
    	$admin->tel = $request->tel;
    	$admin->email = $request->email;
    	$admin->password = md5($request->password);
    	$admin->save();

        return redirect()->route('admin_login');
    }

    public function adminlogin(){
		return view('admin.login');
    }

    public function adminlogout(){
    	session_start();
    	session_destroy();
		return view('admin.login');
    }

    public function check(Request $request)
    {

        $this->validate($request, [
                    'email' => 'required',
                    'password' => 'required'
                    ]);

    	$username_log = $request->email;
    	$password_log = md5($request->password);


    	$admin = DB::table('admins')->where('email', '=', $username_log)->first();
    	$username_db = $admin->email; 	
    	$password_db = $admin->password;
        $fname = $admin->firstname;


    	if ($password_db == $password_log && $username_log == $username_db){

    		session_start();
    		$_SESSION["username"] = $fname;
        	return redirect()->route('new_cont');
    	}	
    	else{
    		return("False");
    	}
    }

    public function viewmembers(){

        $members = DB::table('members')->get();
        $activemember = DB::table('members')->where('active', '=', 1)->get();
        $inactivemember = DB::table('members')->where('active', '=', 0)->get();


        return view('admin.viewmembers',compact('members','activemember','inactivemember'));

    }

    public function viewactivate(){

        $members = DB::table('members')->get();

        return view('admin.viewactivate', ['members' => $members]);

    }

    public function activate($id)
    {

        $member = DB::table('members')->where('id', '=', $id)->first();

        $now = Carbon::now();
        $input = $now->addMonth()->toDateTimeString();

        DB::table('members')
            ->where('id', $id)
            ->update(['timespan' => $input]);

        $act = Member::findOrFail($id);
        $act->active = "1";
        $act->save();

        $alltime = new Alltime;
        $alltime->firstname = $member->firstname;
        $alltime->lastname = $member->lastname;
        $alltime->tel = $member->tel;
        $alltime->email = $member->email;
        $alltime->save();

        return redirect()->route('view_members');
    }

    public function deactivate($id)
    {
        $act = Member::findOrFail($id);

        $act->active = "0";

        $act->save();

        return redirect()->route('view_members');
    }

    public function delete($id)
    {
        $content = Member::findOrFail($id);

        $content->delete();

        return redirect()->route('view_members');
    }

    public function editadmin($id)
    {
        $admin = DB::table('admins')->where('id', '=', $id)->first();

        return view('admin.adminprofile',compact('admin'));
    }

    public function adminprofile($id)
    {
        $admin = DB::table('admins')->where('id', '=', $id)->first();

        return view('admin.profile',compact('admin'));
    }

    public function adminupdate($id, Request $request)
    {
        $content = Admin::findOrFail($id);

        $input = $request->all();

        $content->fill($input)->save();

        Session::flash('flash_message', 'You have been successfully updated your profile!');

        return redirect()->route('adminprofile',$id);
    }
}
