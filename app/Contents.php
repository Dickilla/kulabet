<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contents extends Model
{
    protected $table = 'contents';
    
    protected $fillable = [
        'gameID', 'country', 'league', 'teamone', 'teamtwo',
        'category', 'outcome', 'freetip', 'popular', 'best', 'won',
    ];

    protected $primaryKey = 'gameID';
}
