<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alltime extends Model
{
    protected $table = 'alltime';

    protected $fillable = [
        'firstname', 'lastname', 'tel', 'email',
        ];
}
