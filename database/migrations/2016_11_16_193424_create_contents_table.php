<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
        {
            Schema::create('contents', function($table)
            {
                $table->increments('gameID');
                $table->string('country');
                $table->string('league');
                $table->string('teamone');
                $table->string('teamtwo');
                $table->string('category');
                $table->string('outcome');
                $table->tinyInteger('freetip');
                $table->tinyInteger('popular');
                $table->tinyInteger('best');
                $table->tinyInteger('won');
                $table->timestamps();
            });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
