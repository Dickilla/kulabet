    //This is the used to validate the register page
      $(document).ready(function() {
        $('#register').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            firstname: {
                    message: 'The First name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The First name is required and cannot be empty'
                        },
                        stringLength: {
                            min: 4,
                            max: 30,
                            message: 'The First name must be more than 2 and less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The First name can only consist of alphabetical'
                        }
                    }
                },
            lastname: {
                    message: 'The Last name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The last name is required and cannot be empty'
                        },
                        stringLength: {
                            min: 4,
                            max: 30,
                            message: 'The last name must be more than 2 and less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]+$/,
                            message: 'The last name can only consist of alphabetical'
                        }
                    }
                },
            tel: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter Phone Number'
                        },
                        digits: {
                            message: 'The phone number is not valid'
                        },
                        stringLength: {
                            min: 10,
                            max: 12,
                            message: 'Must be 10 digits'
                        }
                    }
                },
            email: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter Your Email'
                        },
                        emailAddress: {
                            message: 'Please Enter a valid Email'
                        }
                    }
                },
           password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'email',
                        message: 'The password cannot be your Email'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            },
           password_confirmation: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password does not match'
                    },
                }
            },
              }
          });
      });