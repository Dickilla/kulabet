
        $('.viewmember').DataTable({
            select:true,
        });

        $('.freepop').DataTable({
            select:true,
        });

        $('.wonbest').DataTable({
            select:true,
        });
        
        $('.viewcont').DataTable({
            select:true,
        });
        
        $('.viewtip').DataTable({
            select:true,
        });



              $(document).ready(function() {
                $('#addcont').bootstrapValidator({
                  message: 'This value is not valid',
                  feedbackIcons: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                    country: {
                            message: 'The country is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The country is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The country must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    league: {
                            message: 'The league is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The league is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The league must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    teamone: {
                            message: 'The teamone is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The teamone is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The teamone must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    teamtwo: {
                            message: 'The teamtwo is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The teamtwo is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The teamtwo must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    category: {
                            message: 'The category is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The category is required and cannot be empty'
                                },
                            }
                        },
                      }
                  });
              });


              $(document).ready(function() {
                $('#addtip').bootstrapValidator({
                  message: 'This value is not valid',
                  feedbackIcons: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                    cat_name: {
                            message: 'The cat_name is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The cat_name is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 4,
                                    max: 30,
                                    message: 'The cat_name must be more than 2 and less than 30 characters long'
                                },
                            }
                        },
                    cat_alias: {
                            message: 'The cat_alias is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The cat_alias is required and cannot be empty'
                                },
                            }
                        },
                      }
                  });
              });