<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


//Route::get('/', function () {
//   return view('welcome');
//});
Route::get('index','MembersController@index')->name('index');

Route::get('default', function () {
    return view('layouts.default');
});

Route::get('master', function () {
    return view('layouts.master');
});

//home
Route::get('/','ContentsController@home')->name('home');
Route::get('adm/home','AdminThingsController@adminhome')->name('adminhome');
Route::get('mem/home','MembersController@memberhome')->name('memberhome');

Route::get('outdetails/{gameID}/','ContentsController@outdetails')->name('outdetails');

//contents
Route::group(['middleware' => 'admin'], function(){
	Route::get('adminaddcont', 'ContentsController@newcont')->name('new_cont');
	Route::post('createcont','ContentsController@store')->name('create_cont');
	Route::get('viewcont','ContentsController@viewcont')->name('view_cont');
	Route::get('game{gameID}','ContentsController@editcont')->name('edit_cont');
	Route::post('cont/{gameID}/update','ContentsController@update')->name('update_cont');
	Route::post('delete/{gameID}','ContentsController@delete')->name('delete_cont');

	Route::get('addtip', 'ContentsController@addtip')->name('add_tip');
	Route::post('createtip','ContentsController@storetip')->name('create_tip');
	Route::get('viewtip','ContentsController@viewtip')->name('view_tip');
	Route::post('deletetip/{id}','ContentsController@deletetip')->name('delete_tip');

	Route::get('viewmessage','MessageContoller@viewmessage')->name('view_message');
	Route::get('read/{id}/','MessageContoller@read')->name('read');
	Route::post('deletemsg/{id}','MessageContoller@deletemsg');
});

Route::get('contact','MessageContoller@contact')->name('contact');
Route::post('messagestore','MessageContoller@messagestore')->name('messagestore');

//members

Route::get('autodeactivate/{id}','MembersController@autodeactivate')->name('autodeactivate');

Route::group(['middleware' => 'CheckIfExpired'], function(){
	Route::get('details/{gameID}/','ContentsController@details')->name('details');
	Route::get('newmember', 'MembersController@newmember')->name('new_member');
	Route::post('createmember','MembersController@store');
	Route::get('memberlogin', 'MembersController@memberlogin')->name('member_login');
	Route::post('membercheck','MembersController@check');
	Route::get('memberlogout','MembersController@memberlogout')->name('member_logout');


	Route::get('viewgames','MembersController@view')->name('view_games');
	Route::get('viewgame','MembersController@viewgame')->name('view_game');
	Route::get('viewtips','MembersController@viewtips')->name('view_tips');
	Route::get('viewpopular','MembersController@viewpopular')->name('view_popular');
	Route::get('viewbest','MembersController@viewbest')->name('view_best');
	Route::get('viewwon','MembersController@viewwon')->name('view_won');
});

Route::get('memberprofile/{id}','MembersController@memberprofile')->name('memberprofile');
Route::post('memberupdate/{id}','MembersController@memberupdate')->name('memberupdate');



//Admin
Route::group(['middleware' => 'admin'], function(){
	Route::get('newadmin', 'AdminThingsController@newadmin')->name('new_admin');
	Route::post('createadmin','AdminThingsController@store');
	Route::get('adminlogin', 'AdminThingsController@adminlogin')->name('admin_login');
	Route::post('admincheck','AdminThingsController@check');
	Route::get('adminlogout','AdminThingsController@adminlogout')->name('admin_logout');
	Route::get('adminprofile/{id}','AdminThingsController@adminprofile')->name('adminprofile');
	Route::get('editadmin/{id}','AdminThingsController@editadmin')->name('editadmin');
	Route::post('editadmin/{id}/update','AdminThingsController@adminupdate')->name('adminupdate');

	Route::get('viewmembers','AdminThingsController@viewmembers')->name('view_members');
	Route::post('deletemember/{id}','AdminThingsController@delete')->name('delete_member');
	Route::get('viewactivate','AdminThingsController@viewactivate')->name('view_activate');
	Route::get('activate/{id}','AdminThingsController@activate')->name('acti_vate');
	Route::get('deactivate/{id}','AdminThingsController@deactivate')->name('deacti_vate');

	//AdminSwitch
	Route::get('freepopular','AdminSwitch@freepopular')->name('freepopular');
	Route::get('actfreetip/{gameID}','AdminSwitch@actfreetip')->name('acti_freetip');
	Route::get('deactfreetip/{gameID}','AdminSwitch@deactfreetip')->name('deacti_freetip');

	Route::get('actpopular/{gameID}','AdminSwitch@actpopular')->name('acti_popupar');
	Route::get('deactpopular/{gameID}','AdminSwitch@deactpopular')->name('deacti_popular');

	Route::get('wonbest','AdminSwitch@wonbest')->name('wonbest');
	Route::get('actwon/{gameID}','AdminSwitch@actwon')->name('acti_won');
	Route::get('deactwon/{gameID}','AdminSwitch@deactwon')->name('deacti_won');

	Route::get('actbest/{gameID}','AdminSwitch@actbest')->name('acti_best');
	Route::get('deactbest/{gameID}','AdminSwitch@deactbest')->name('deacti_best');

	Route::get('hide/{id}','GamesController@hide')->name('hide');
	Route::get('unhide/{id}','GamesController@unhide')->name('unhide');
});

Route::group(['middleware' => 'CheckIfExpired'], function(){
	Route::get('onefive','GamesController@onefive')->name('onefive');
	Route::get('twofive','GamesController@twofive')->name('twofive');
	Route::get('double','GamesController@double')->name('double');
	Route::get('firsthalf','GamesController@firsthalf')->name('firsthalf');
	Route::get('sportpesa','GamesController@sportpesa')->name('sportpesa');

	Route::get('awaywin','GamesController@awaywin')->name('awaywin');
	Route::get('homewin','GamesController@homewin')->name('homewin');
	Route::get('draw','GamesController@draw')->name('draw');
	Route::get('correct','GamesController@correct')->name('correct');
	Route::get('megajackport','GamesController@megajackport')->name('megajackport');
	Route::get('goalgoal','GamesController@goalgoal')->name('goalgoal');
});

//Admin Login
Route::get('admin/login', 'AdminAuth\LoginController@showLoginForm')->name('admlogin');
Route::post('admin/login', 'AdminAuth\LoginController@login');
Route::post('admin/logout', 'AdminAuth\LoginController@logout');

//Admin Register
Route::get('admin/register', 'AdminAuth\RegisterController@showRegistrationForm');
Route::post('admin/register', 'AdminAuth\RegisterController@register');

//Admin Passwords
Route::post('admin/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
Route::post('admin/password/reset', 'AdminAuth\ResetPasswordController@reset');
Route::get('admin/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
Route::get('admin/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');


//Member Login
Route::get('member/login', 'MemberAuth\LoginController@showLoginForm')->name('memlogin');
Route::post('member/login', 'MemberAuth\LoginController@login');
Route::post('member/logout', 'MemberAuth\LoginController@logout');

//Member Register
Route::get('member/register', 'MemberAuth\RegisterController@showRegistrationForm')->name('memreg');
Route::post('member/register', 'MemberAuth\RegisterController@register');

//Member Passwords
Route::post('member/password/email', 'MemberAuth\ForgotPasswordController@sendResetLinkEmail');
Route::post('member/password/reset', 'MemberAuth\ResetPasswordController@reset');
Route::get('member/password/reset', 'MemberAuth\ForgotPasswordController@showLinkRequestForm');
Route::get('member/password/reset/{token}', 'MemberAuth\ResetPasswordController@showResetForm');
